// This module is based on ISO 8601.

import IsBoolean   from '@wingnut/IsBoolean'
import Spell       from '@wingnut/Spell'



export default Spell.Guard(`DateTimeUtcIso`,

	{
		date: {
			Default: () => new Date(),
			instanceof: Date,
		},
		isBasicFormat: {
			Default: () => false,
			IsBoolean,
		},
	},

	({ date, isBasicFormat }) => {

		let dateTime = (
			        String(date.getUTCFullYear() ) // eslint-disable-line no-mixed-spaces-and-tabs
			+ '-' + String(date.getUTCMonth() + 1).padStart(2, '0')
			+ '-' + String(date.getUTCDate()     ).padStart(2, '0')
			+ 'T' + String(date.getUTCHours()    ).padStart(2, '0')
			+ ':' + String(date.getUTCMinutes()  ).padStart(2, '0')
			+ ':' + String(date.getUTCSeconds()  ).padStart(2, '0')
			+ 'Z'
		)

		if ( isBasicFormat ) {
			dateTime = dateTime.replaceAll(/[-:]/g, '')
		}

		return dateTime

	}

)

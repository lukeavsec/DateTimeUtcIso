import Assert         from '/wingnut/Assert/index.js'
import DateTimeUtcIso from './index.js'
import IsFunction     from '/wingnut/IsFunction/index.js'
import IsString       from '/wingnut/IsString/index.js'
import Test           from '/wingnut/Muster/index.js'



const testDate          = new Date(1700762072256)
const testDateTime      = '2023-11-23T17:54:32Z'
const testDateTimeBasic = '20231123T175432Z'



Test('is a function', () => {
	Assert(IsFunction(DateTimeUtcIso))
})

Test('works without an argument', () => {
	DateTimeUtcIso()
})

Test('returns a string', () => {
	Assert(IsString(DateTimeUtcIso()))
})

Test('string is in ISO 8601 format', () => {
	Assert(DateTimeUtcIso({ date:testDate }) === testDateTime)
})

Test('isBasicFormat option', () => {
	Assert(DateTimeUtcIso({ date:testDate, isBasicFormat:true }) === testDateTimeBasic)
})
